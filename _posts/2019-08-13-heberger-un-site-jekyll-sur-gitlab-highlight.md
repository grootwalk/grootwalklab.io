---
layout: post
title:  "Héberger un site Jekyll sur Gitlab"
category: [web]
tags: [web, site, jekyll, blog, static, gitlab, forge, logiciel]
---

Pour héberger un site web [Jekyll](https://jekyllrb.com/) sur Gitlab, voici les différentes étapes à réaliser :

- créer un nouveau projet sur Gitlab à partir d'un template `pages/Jekyll`

- pour que le site web soit héberger à l'adresse `username.gitlab.io`, modifier
  le nom du projet en : `username.gitlab.io`

- pour que la redirection des URLs se fasse correctement, modifier les entrées
  suivantes dans le fichier `_config.yml` :

{% highlight yaml %}
baseurl: "" 
url: "https://username.gitlab.io" 
{% endhighlight %}

- activer le site web en allant dans "CI/CD" (menu de gauche), option
  "pipelines" et cliquer sur le bouton `Run pipeline` 